python3 -m venv venv
source venv/bin/activate
pip3 install poetry
pip3 install chardet
poetry install
python3 manage.py migrate
python3 manage.py collectstatic --no-input
coverage run --source='.' manage.py test
deactivate
